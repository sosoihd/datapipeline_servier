FROM python:3.7-slim

RUN mkdir -p /opt/dagster/dagster_home /opt/dagster/app

# Copy your pipeline code and workspace to /opt/dagster/app
# COPY pipelines.py workspace.yaml /opt/dagster/app/

ADD ./  /opt/dagster/app/

ENV DAGSTER_HOME=/opt/dagster/dagster_home/

ENV DAGSTER_APP=/opt/dagster/app/

ENV DAGSTER_SHARE=/opt/dagster/app/shared_folder

ENV DAGSTER_CONFIG=/opt/dagster/app/shared_folder/config

# Copy dagster instance YAML to $DAGSTER_HOME
COPY dagster.yaml /opt/dagster/dagster_home/

WORKDIR /opt/dagster/app


RUN pip3 install .

EXPOSE 3000

CMD ["dagit", "-h", "127.0.0.1", "-p", "3000"]
