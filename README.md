# data_pipeline_servier


### Contents

| Name | Description |
|-|-|
| `data_pipeline_servier/` | A Python module that contains code for your Dagster repository |
| `data_pipeline_servier_tests/` | A Python module that contains tests for `data_pipeline_servier` |
| `workspace.yaml` | A file that specifies the location of the user code for Dagit and the Dagster CLI |
| `README.md` | A description and guide for this code repository |
| `setup.py` | A build script with Python package dependencies for this code repository |
| `Dockerfile` | A docker file to build a python based image and install the package |
| `.gitlab-ci.yml` | A yml file to build the CI/CD pipeline to build the docker image, test and run the code |
| `dagster.yaml` | A yaml file to configure Dagster instance | 

## Getting up and running the pipeline in local

1. Create a new Python environment and activate.

**Pyenv**
```bash
export PYTHON_VERSION=X.Y.Z
pyenv install $PYTHON_VERSION
pyenv virtualenv $PYTHON_VERSION data_pipeline_servier
pyenv activate data_pipeline_servier
```

**Conda**
```bash
export PYTHON_VERSION=X.Y.Z
conda create --name data_pipeline_servier python=PYTHON_VERSION
conda activate data_pipeline_servier
```

2. Once you have activated your Python environment, install your repository as a Python package. By
using the `--editable` flag, `pip` will install your repository in
["editable mode"](https://pip.pypa.io/en/latest/reference/pip_install/?highlight=editable#editable-installs)
so that as you develop, local code changes will automatically apply.

```bash
pip install --editable .
```

## Local Development

1. Start the [Dagit process](https://docs.dagster.io/overview/dagit). This will start a Dagit web
server that, by default, is served on http://localhost:3000.

```bash
dagit
```

2. (Optional) If you want to enable Dagster
[Schedules](https://docs.dagster.io/overview/schedules-sensors/schedules) or
[Sensors](https://docs.dagster.io/overview/schedules-sensors/sensors) for your pipelines, start the
[Dagster Daemon process](https://docs.dagster.io/overview/daemon#main) **in a different shell or terminal**:

```bash
dagster-daemon run
```

## Local Testing

Tests can be found in `data_pipeline_servier_tests` and are run with the following command:

```bash
pytest data_pipeline_servier_tests
```

As you create Dagster solids and pipelines, add tests in `data_pipeline_servier_tests/` to check that your
code behaves as desired and does not break over time.

[For hints on how to write tests for solids and pipelines in Dagster,
[see our documentation tutorial on Testing](https://docs.dagster.io/tutorial/testable).

# CI/CD Pipeline :

The CI/CD pipeline is designed to demonstrate that datapipeline is working.
We made sure to adapt the test so that it runs in the Gitlab environment.

## Stage :
### Build :
We use the Docker file to build and image based on Python image, add the package and install it using pip.
We push the image into gitlab registry and tag it with commit id.
### Test :
We pull the docker image and run tests on solids and pipelines using this command :

```bash
pytest data_pipeline_servier_tests
```
### Run :
We pull the docker image and run the pipeline using the input files and configuration files.
We use the `shared_folder` to share data between gitlab repository and our container.
The outputs are displayed with the job logs.
