from dagster import ModeDefinition, pipeline
from data_pipeline_servier.solids.load_drugs import load_drugs
from data_pipeline_servier.solids.load_pubmed import load_pubmed
from data_pipeline_servier.solids.load_clinical_trials import load_clinical_trials
from data_pipeline_servier.solids.clean_drugs import clean_drugs
from data_pipeline_servier.solids.clean_pubmed import clean_pubmed
from data_pipeline_servier.solids.clean_clinical_trials import clean_clinical_trials
from data_pipeline_servier.solids.transform_data import transform_data
from data_pipeline_servier.solids.persist_result import persist_result



# Mode definitions allow you to configure the behavior of your pipelines and solids at execution
# time. For hints on creating modes in Dagster, see our documentation overview on Modes and
# Resources: https://docs.dagster.io/overview/modes-resources-presets/modes-resources

MODE_DEV = ModeDefinition(name="dev", resource_defs={})
MODE_TEST = ModeDefinition(name="test", resource_defs={})


@pipeline(mode_defs=[MODE_DEV, MODE_TEST])
def data_process():
    """
    This Pypeline reads the sources data, makes transformations and write results in JSON file.
    """
    drugs=load_drugs()
    pubMed=load_pubmed()
    clinicalTrials=load_clinical_trials()

    drugs = clean_drugs(drugs)
    pubMed = clean_pubmed(pubMed)
    clinicalTrials = clean_clinical_trials(clinicalTrials)


    transformedData = transform_data(drugs, pubMed, clinicalTrials)

    persist_result(transformedData)
