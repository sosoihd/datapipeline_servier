from dagster import ModeDefinition, pipeline
from data_pipeline_servier.solids.load_drugs_mention import load_drugs_mention
from data_pipeline_servier.solids.extraction import extraction
from data_pipeline_servier.solids.persist_mentions import persist_mentions



# Mode definitions allow you to configure the behavior of your pipelines and solids at execution
# time. For hints on creating modes in Dagster, see our documentation overview on Modes and
# Resources: https://docs.dagster.io/overview/modes-resources-presets/modes-resources

MODE_DEV = ModeDefinition(name="dev", resource_defs={})
MODE_TEST = ModeDefinition(name="test", resource_defs={})


@pipeline(mode_defs=[MODE_DEV, MODE_TEST])
def extract_journal():
    """
    This Pypeline reads the results JSON and extract the journal that cited the greatest number of drugs
    """
    drugsMention=load_drugs_mention()
    
    journalDrugs=extraction(drugsMention)

    persist_mentions(journalDrugs)

    
