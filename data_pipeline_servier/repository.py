from dagster import repository

from data_pipeline_servier.pipelines.data_process import data_process
from data_pipeline_servier.pipelines.extract_journal import extract_journal
from data_pipeline_servier.schedules.my_hourly_schedule import my_hourly_schedule
from data_pipeline_servier.sensors.my_sensor import my_sensor
from data_pipeline_servier.solids import load_drugs


@repository
def data_pipeline_servier():
    """
    The repository definition for this data_pipeline_servier Dagster repository.

    For hints on building your Dagster repository, see our documentation overview on Repositories:
    https://docs.dagster.io/overview/repositories-workspaces/repositories
    """
    pipelines = [data_process,extract_journal]
    schedules = [my_hourly_schedule]
    sensors = [my_sensor]

    return pipelines + schedules + sensors
