from dagster import solid, OutputDefinition, InputDefinition
from dagster_pandas import create_dagster_pandas_dataframe_type, PandasColumn, DataFrame
import pandas as pd
import os, sys






@solid(input_defs=[InputDefinition(name="drugs",dagster_type=pd.DataFrame)],
    output_defs=[OutputDefinition(dagster_type=pd.DataFrame)])
def clean_drugs(context,drugs) :
    """
    This task is used to clean data.
    We starts by putting the drugs name in lower case.
    This will help to easily find them in the publications titles and clinical trials.
    """
    # Lowercase data
    drugs['drug']=drugs['drug'].str.lower()

    # Delete special characters
    #TODO
    return drugs
