from dagster import solid, OutputDefinition, InputDefinition
from dagster import solid, OutputDefinition, InputDefinition
from dagster_pandas import create_dagster_pandas_dataframe_type, PandasColumn, DataFrame
import pandas as pd
import os, sys





@solid(input_defs=[InputDefinition(name="pubMed",dagster_type=pd.DataFrame)],
    output_defs=[OutputDefinition(dagster_type=pd.DataFrame)])
def clean_pubmed(context,pubMed) :
    """
    This task is used to clean data.
    We starts by putting the title and the journal name in lower case.
    This will help to easily compare journals and find drugs in titles.
    """
    # Lowercase data
    pubMed['title']=pubMed['title'].str.lower()
    pubMed['journal']=pubMed['journal'].str.lower()

    # Delete special characters
    #TODO
    return pubMed