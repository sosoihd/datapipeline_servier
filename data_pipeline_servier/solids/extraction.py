from dagster import solid, OutputDefinition
import pandas as pd
import os


@solid
def extraction(context,drugsMention) -> pd.Series:

    drugsMention = (drugsMention.groupby('Journal', sort=False)['Drug']
          .nunique()
          .reset_index()
          .rename(columns={'Drug':'drugs_count'})
          .reindex(columns=['Journal', 'drugs_count']))

    drugsMention.sort_values(by=['drugs_count'], ascending=False, inplace=True)

    print(drugsMention.iloc[drugsMention['drugs_count'].idxmax()])
    
    return drugsMention.iloc[drugsMention['drugs_count'].idxmax()]
