from dagster import solid, OutputDefinition
from dagster_pandas import create_dagster_pandas_dataframe_type, PandasColumn, DataFrame
import pandas as pd
import os, sys

ClinicalTrialsDataframe = create_dagster_pandas_dataframe_type(
    name="ClinicalTrialsDataframe",
    columns=[
        PandasColumn("id"),
        PandasColumn("scientific_title"),
        PandasColumn("date"),
        PandasColumn("journal"),
    ],
)

@solid(output_defs=[OutputDefinition(name="ClinicalTrialsDataframe", dagster_type=ClinicalTrialsDataframe)],config_schema={'dataPath' : str, 'fileName' : str})
def load_clinical_trials(context) -> DataFrame:
    """
    This task is used to load pubmed file containing the titles of the differents clinical trials, the journal and the date of publication.
    We also have a unique id to identify one row.
    """

    # Build file path
    dataPath = context.solid_config["dataPath"]
    clinicalTrialsPathNameFile = context.solid_config["fileName"]
    clinicalTrialsFilePath = os.path.sep.join([dataPath, clinicalTrialsPathNameFile])

    # Check file type (csv or json)
    fileFormat = clinicalTrialsFilePath.split('.')[-1]

    if fileFormat == "csv" :
        clinicalTrials = pd.read_csv(clinicalTrialsFilePath,sep=',')
    elif fileFormat == "json" :
        clinicalTrials = pd.read_json(clinicalTrialsFilePath,convert_dates=False)
    else :
        raise TypeError("The file format {} is not supported. Use CSV or JSON instead !!".format(fileFormat))

    return clinicalTrials
