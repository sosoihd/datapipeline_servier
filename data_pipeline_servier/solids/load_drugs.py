from dagster import solid, OutputDefinition
from dagster_pandas import create_dagster_pandas_dataframe_type, PandasColumn, DataFrame
import pandas as pd
import os, sys

DrugsDataframe = create_dagster_pandas_dataframe_type(
    name="DrugsDataframe",
    columns=[
        PandasColumn("atccode"),
        PandasColumn("drug"),
    ],
)

@solid(output_defs=[OutputDefinition(name="DrugsDataframe", dagster_type=DrugsDataframe)],config_schema={'dataPath' : str, 'fileName' : str})
def load_drugs(context) -> DataFrame:
    """
    this task is used to load drugs file containing the name of drug and and id called atccode
    """

    # Build file path
    dataPath = context.solid_config["dataPath"]
    drugsNameFile = context.solid_config["fileName"]
    drugsFilePath = os.path.sep.join([dataPath, drugsNameFile])

    # Check file type (csv or json)
    fileFormat = drugsFilePath.split('.')[-1]

    if fileFormat == "csv" :
        drugs = pd.read_csv(drugsFilePath,sep=',')
    elif fileFormat == "json" :
        drugs = pd.read_json(drugsFilePath,convert_dates=False)
    else :
        raise TypeError("The file format {} is not supported. Use CSV or JSON instead !!".format(fileFormat))

    return drugs
