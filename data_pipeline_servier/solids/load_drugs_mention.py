from dagster import solid, OutputDefinition
from dagster_pandas import create_dagster_pandas_dataframe_type, PandasColumn
import pandas as pd
import os

DrugsMentionDataframe = create_dagster_pandas_dataframe_type(
    name="DrugsMentionDataframe",
    columns=[
        PandasColumn("Journal"),
        PandasColumn("Drug"),
        PandasColumn("Publication"),
        PandasColumn("Date"),
    ],
)

@solid(output_defs=[OutputDefinition(name="DrugsMentionDataframe", dagster_type=DrugsMentionDataframe)],config_schema={'dataPath' : str, 'fileName' : str})
def load_drugs_mention(context) -> pd.DataFrame:

    # Build file path
    dataPath = context.solid_config["dataPath"]
    fileName = context.solid_config["fileName"]
    filePath = os.path.sep.join([dataPath, fileName])

    # Check file type (csv or json)
    fileFormat = filePath.split('.')[-1]

    
    if fileFormat == "csv" :
        drugsMention = pd.read_csv(filePath,sep=',')
    elif fileFormat == "json" :
        drugsMention = pd.read_json(filePath,convert_dates=False,orient="records",lines=True)
    else :
        raise TypeError("The file format {} is not supported. Use CSV or JSON instead !!".format(fileFormat))

    return drugsMention
