from dagster import solid, OutputDefinition
from dagster_pandas import create_dagster_pandas_dataframe_type, PandasColumn, DataFrame
import pandas as pd
import os, sys

PubMedDataframe = create_dagster_pandas_dataframe_type(
    name="PubMedDataframe",
    columns=[
        PandasColumn("id"),
        PandasColumn("title"),
        PandasColumn("date"),
        PandasColumn("journal"),
    ],
)

@solid(output_defs=[OutputDefinition(name="PubMedDataframe", dagster_type=PubMedDataframe)],config_schema={'dataPath' : str, 'fileName' : str})
def load_pubmed(context) -> DataFrame:
    """
    This task is used to load pubmed file containing the titles of the differents articles, the journal and the date of publication.
    We also have a unique id to identify one row.
    """

    # Build file path
    dataPath = context.solid_config["dataPath"]
    pubmedPathNameFile = context.solid_config["fileName"]
    pubmedPathFilePath = os.path.sep.join([dataPath, pubmedPathNameFile])

    # Check file type (csv or json)
    fileFormat = pubmedPathFilePath.split('.')[-1]

    if fileFormat == "csv" :
        pubmed = pd.read_csv(pubmedPathFilePath,sep=',')
    elif fileFormat == "json" :
        pubmed = pd.read_json(pubmedPathFilePath,convert_dates=False)
    else :
        raise TypeError("The file format {} is not supported. Use CSV or JSON instead !!".format(fileFormat))

    return pubmed
