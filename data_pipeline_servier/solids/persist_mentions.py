from dagster import solid, OutputDefinition, InputDefinition
import pandas as pd
import os, sys



@solid(input_defs=[InputDefinition(name="drugsMention",dagster_type=pd.Series)])
def persist_mentions(context, drugsMention) :
    """
    This task is used to persist results in JSON file.
    We use New Line Delimited JSON so the file can be ingested in Bigquery.
    Ps: it's the only JSON format accepted by Bigquery.
    """
    dataPath = context.solid_config["dataPath"]
    drugsNameFile = context.solid_config["fileName"]
    resultFilePath = os.path.sep.join([dataPath, drugsNameFile])
    drugsMention.to_json(resultFilePath)
    
    

