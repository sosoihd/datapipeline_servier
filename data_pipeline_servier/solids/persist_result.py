from dagster import solid, OutputDefinition, InputDefinition
import pandas as pd
import os, sys



@solid(input_defs=[InputDefinition(name="transformedData",dagster_type=pd.DataFrame)])
def persist_result(context, transformedData) :
    """
    This task is used to persist results in JSON file.
    We use New Line Delimited JSON so the file can be ingested in Bigquery.
    Ps: it's the only JSON format accepted by Bigquery.
    """
    dataPath = context.solid_config["dataPath"]
    drugsNameFile = context.solid_config["fileName"]
    resultFilePath = os.path.sep.join([dataPath, drugsNameFile])
    transformedData.to_json(resultFilePath,orient='records',lines=True)
    

