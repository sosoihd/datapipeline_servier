from dagster import solid, OutputDefinition, InputDefinition
from dagster_pandas import create_dagster_pandas_dataframe_type, PandasColumn, DataFrame
import pandas as pd
import os, sys



@solid(input_defs=[InputDefinition(name="drugs",dagster_type=pd.DataFrame), InputDefinition(name="pubMed",dagster_type=pd.DataFrame), InputDefinition(name="clinicalTrials",dagster_type=pd.DataFrame)],
    output_defs=[OutputDefinition(dagster_type=pd.DataFrame)])
def transform_data(context, drugs, pubMed, clinicalTrials) -> pd.DataFrame:
    """
    This task is used to put data together and find the publications that mention our drugs.
    The result is formed to be easily ingested in relational data base.
    """

    transformedDataframe=pd.DataFrame(columns=['Journal','Drug','Publication','Date'])
    for elt in drugs['drug']:
        matchsPubMed = pubMed[pubMed['title'].str.contains(elt)]
        for ind_ligne, contenu_ligne in matchsPubMed.iterrows():
            transformedDataframe=transformedDataframe.append(pd.DataFrame([[contenu_ligne["journal"],elt,contenu_ligne["title"],contenu_ligne["date"]]],columns=['Journal','Drug','Publication','Date']))
        matchsClinicalTrials = clinicalTrials[clinicalTrials['scientific_title'].str.contains(elt)]
        for ind_ligne, contenu_ligne in matchsClinicalTrials.iterrows():
            transformedDataframe=transformedDataframe.append(pd.DataFrame([[contenu_ligne["journal"],elt,contenu_ligne["scientific_title"],contenu_ligne["date"]]],columns=['Journal','Drug','Publication','Date']))
        
    transformedDataframe=transformedDataframe.reset_index(drop=True)
    
    return transformedDataframe