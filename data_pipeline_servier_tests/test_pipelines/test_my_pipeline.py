from dagster import execute_pipeline
from data_pipeline_servier.pipelines.data_process import data_process
from data_pipeline_servier.pipelines.extract_journal import extract_journal
from data_pipeline_servier.solids.load_pubmed import load_pubmed
from data_pipeline_servier.solids.load_clinical_trials import load_clinical_trials
from data_pipeline_servier.solids.clean_drugs import clean_drugs
from data_pipeline_servier.solids.clean_pubmed import clean_pubmed
from data_pipeline_servier.solids.clean_clinical_trials import clean_clinical_trials
from data_pipeline_servier.solids.transform_data import transform_data
from data_pipeline_servier.solids.persist_result import persist_result
from data_pipeline_servier.solids.load_drugs_mention import load_drugs_mention
from data_pipeline_servier.solids.extraction import extraction
from data_pipeline_servier.solids.persist_mentions import persist_mentions


run_config = {
    "solids": {
        "load_drugs": {
            "config": {
                "dataPath": "/builds/sosoihd/datapipeline_servier/shared_folder/data/input", 
                "fileName": "drugs.csv"
            }
        },

        "load_pubmed": {
            "config": {
                "dataPath": "/builds/sosoihd/datapipeline_servier/shared_folder/data/input", 
                "fileName": "pubmed.csv"
            }
        },

        "load_clinical_trials": {
            "config": {
                "dataPath": "/builds/sosoihd/datapipeline_servier/shared_folder/data/input", 
                "fileName": "clinical_trials.csv"
            }
        },

        "persist_result": {
            "config": {
                "dataPath": "/builds/sosoihd/datapipeline_servier/shared_folder/data/output", 
                "fileName": "drugs_mention.json"
            }
        },
    }
}


run_config2 = {
    "solids": {
        "load_drugs_mention": {
                "config": {
                    "dataPath": "/builds/sosoihd/datapipeline_servier/shared_folder/data/output", 
                    "fileName": "drugs_mention.json"
                }
            },

        "persist_mentions": {
            "config": {
                "dataPath": "/builds/sosoihd/datapipeline_servier/shared_folder/data/output", 
                "fileName": "journalDrugs.json"
                }
            },
    }
}


def test_my_pipeline():
    """
    This is an example test for a Dagster pipeline.

    For hints on how to test your Dagster pipelines, see our documentation tutorial on Testing:
    https://docs.dagster.io/tutorial/testable
    """
    result = execute_pipeline(data_process, mode="test",run_config=run_config)

    assert result.success


def test_my_pipeline2():
    """
    This is an example test for a Dagster pipeline.

    For hints on how to test your Dagster pipelines, see our documentation tutorial on Testing:
    https://docs.dagster.io/tutorial/testable
    """
    result = execute_pipeline(extract_journal, mode="test",run_config=run_config2)

    assert result.success