from dagster import execute_solid
from data_pipeline_servier.pipelines.data_process import MODE_TEST
from data_pipeline_servier.solids.load_drugs import load_drugs
from data_pipeline_servier.solids.load_pubmed import load_pubmed
from data_pipeline_servier.solids.load_clinical_trials import load_clinical_trials
from data_pipeline_servier.solids.clean_drugs import clean_drugs
from data_pipeline_servier.solids.clean_pubmed import clean_pubmed
from data_pipeline_servier.solids.clean_clinical_trials import clean_clinical_trials
from data_pipeline_servier.solids.transform_data import transform_data
from data_pipeline_servier.solids.persist_result import persist_result


run_config1 = {
    "solids": {
        "load_drugs": {
            "config": {
                "dataPath": "/builds/sosoihd/datapipeline_servier/shared_folder/data/input", 
                "fileName": "drugs.csv"
            }
        }
    }
}

run_config2 = {
    "solids": {
        "load_pubmed": {
            "config": {
                "dataPath": "/builds/sosoihd/datapipeline_servier/shared_folder/data/input", 
                "fileName": "pubmed.csv"
            }
        }
    }
}

run_config3 = {
    "solids": {
        "load_clinical_trials": {
            "config": {
                "dataPath": "/builds/sosoihd/datapipeline_servier/shared_folder/data/input", 
                "fileName": "clinical_trials.csv"
            }
        }
    }
}

          

def test_loads():
    """
    This is an example test for a Dagster solid.

    For hints on how to test your Dagster solids, see our documentation tutorial on Testing:
    https://docs.dagster.io/tutorial/testable
    """
    result = execute_solid(load_drugs, mode_def=MODE_TEST,run_config=run_config1)
    result = execute_solid(load_pubmed, mode_def=MODE_TEST,run_config=run_config2)
    result = execute_solid(load_clinical_trials, mode_def=MODE_TEST,run_config=run_config3)

    assert result
