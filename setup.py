import setuptools

setuptools.setup(
    name="data_pipeline_servier",
    packages=setuptools.find_packages(exclude=["data_pipeline_servier_tests"]),
    install_requires=[
        "dagster==0.12.1",
        "dagit==0.12.1",
        "pytest",
        "pandas",
        "dagster-pandas"
    ],
)
